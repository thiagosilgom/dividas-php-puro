<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	google.charts.load("current", {packages:["corechart"]});
	google.charts.setOnLoadCallback(drawChart);
	
	function drawChart() {
		<?php if (!empty($list)) : ?>
			drawPieChart();

			<?php if (!empty($barChart)) : ?>
				drawBarChart();
			<?php endif; ?>
		<?php endif; ?>
	}
	
	function drawPieChart() {
		var data = google.visualization.arrayToDataTable([
			['Status', 'R$'],
			<?php foreach ($pieChart as $status => $valor) : ?>
				['<?php echo $status; ?>', <?php echo $valor; ?>],
			<?php endforeach; ?>
		]);

		var options = {
			title: 'STATUS DAS DÍVIDAS',
			is3D: true,
			colors: ['green', 'orange', 'red'],
			legend: { position: 'bottom', alignment: 'start' }
		};

		var chart = new google.visualization.PieChart(document.getElementById('piechart'));
		chart.draw(data, options);
	}

	function drawBarChart() {
		var data = google.visualization.arrayToDataTable([
			['Cliente', 'Qtde'],
			<?php foreach ($barChart as $value) : ?>
				<?php if (isset($value['nome'])) : ?>
					['<?php echo $value['nome']; ?>', <?php echo $value['count']; ?>],
				<?php endif; ?>
			<?php endforeach; ?>
		]);

		var options = {
			title: 'CLIENTES QUE MAIS PAGAM EM DIA'
		};

		var chart = new google.visualization.BarChart(document.getElementById('barchart'));
		chart.draw(data, options);
	}
</script>