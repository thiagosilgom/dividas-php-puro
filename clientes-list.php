<?php include('header.php'); ?>

<div class="table-responsive">
	<table class="table table-striped table-sm">
		<thead>
			<tr>
				<th>#</th>
				<th>Nome</th>
				<th>CPF/CNPJ</th>
				<th>Data de Nascimento</th>
				<th>Ações</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($list as $data) : ?>
				<tr>
					<td><?php echo $data['id']; ?></td>
					<td><?php echo h($data['nome']); ?></td>
					<td><?php echo h($data['cpf_cnpj']); ?></td>
					<td><?php echo showDate($data['data_nascimento']); ?></td>
					<td>
						<a class="btn btn-success" href="<?php echo url(['action' => 'edit', 'id' => $data['id']]); ?>">Editar</a>
						<a class="btn btn-danger" href="<?php echo url(['action' => 'remove', 'id' => $data['id']]); ?>" onclick="if (confirm('Deseja realmente remover este registro? Caso existam dívidas relacionadas a este cliente, também serão excluídas.')) { return true; } return false;">Excluir</a>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>

<?php include('footer.php'); ?>