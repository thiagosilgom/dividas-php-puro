<!doctype html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Desafio Receiv - Thiago da Silva Gomes</title>

		<link href="./assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="./assets/css/style.css" rel="stylesheet">
		<?php if ($includeHead !== null) : ?>
			<?php include($includeHead); ?>
		<?php endif; ?>
 	</head>

 	<body>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse justify-content-md-center" id="navbar">
				<ul class="navbar-nav">
					<li class="nav-item <?php echo ($module == 'dashboard' ? 'active' : ''); ?>">
						<a class="nav-link" href="./">Dashboard</a>
					</li>
					<li class="nav-item dropdown <?php echo ($module == 'clientes' ? 'active' : ''); ?>">
						<a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Clientes</a>
						<div class="dropdown-menu" aria-labelledby="dropdown01">
							<a class="dropdown-item" href="./?module=clientes&action=list">Listagem</a>
							<a class="dropdown-item" href="./?module=clientes&action=create">Novo</a>
						</div>
					</li>
					<li class="nav-item dropdown <?php echo ($module == 'dividas' ? 'active' : ''); ?>">
						<a class="nav-link dropdown-toggle" href="#" id="dropdown02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dívidas</a>
						<div class="dropdown-menu" aria-labelledby="dropdown02">
							<a class="dropdown-item" href="./?module=dividas&action=list">Listagem</a>
							<a class="dropdown-item" href="./?module=dividas&action=create">Nova</a>
						</div>
					</li>
				</ul>
			</div>
		</nav>

		<div class="container">
			<main role="main">
				<?php include('msg.php'); ?>