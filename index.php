<?php
	session_start();

	require_once('./db/connection.php');
	require_once('./class/Dashboard.php');
	require_once('./class/Cliente.php');
	require_once('./class/Divida.php');

	/*
	 * Funções
	 */
	function h($string) {
		return htmlentities($string, ENT_QUOTES);
	}
	function url($params) {
		global $module;
		$result = "./?module={$module}";
		foreach ($params as $param => $value) {
			$result .= "&{$param}={$value}";
		}
		return $result;
	}
	function showDate($date) {
		if (empty($date) || $date == '0000-00-00') {
			return '';
		}
		return date('d/m/Y', strtotime($date));
	}
	function showDatetime($datetime) {
		if (empty($datetime) || $datetime == '0000-00-00 00:00:00') {
			return '';
		}
		$split = explode(' ', $datetime);
		return showDate($split[0]) . ' às ' . $split[1];
	}
	function showMoney($float) {
		return 'R$ ' . number_format($float, 2, ',', '.');
	}

	/*
	 * Encontrando o módulo e a ação
	 */
	$module = null;
	$action = null;
	$id = null;
	if (isset($_GET['module']) && !empty($_GET['module'])) {
		$module = $_GET['module'];
	}
	if (isset($_GET['action']) && !empty($_GET['action'])) {
		$action = $_GET['action'];
	}
	if (isset($_GET['id']) && !empty($_GET['id'])) {
		$id = $_GET['id'];
	}
	switch ($module) {
		case 'clientes':
		case 'dividas':
			if (!in_array($action, ['list', 'create', 'edit', 'remove'])) {
				$action = 'list';
				$id = null;
			}
			if (in_array($action, ['edit', 'remove'])) {
				if (isset($_GET['id']) && !is_numeric($_GET['id'])) {
					$action = 'list';
					$id = null;
				}
			}
			break;
		default:
			$module = 'dashboard';
			$action = 'view';
			$id = null;
	}

	/*
	 * Invocando o método referente à ação
	 */
	switch ($module) {
		case 'dashboard':
			$obj = new Dashboard();
			break;
		case 'clientes':
			$obj = new Cliente();
			break;
		case 'dividas':
			$obj = new Divida();
			break;
	}
	$result = (empty($id) ? $obj->{$action}() : $obj->{$action}($id));
	if (isset($result['redirect'])) {
		header('Location: ' . url($result['redirect']));
		exit();
	}
	if (isset($result['vars'])) {
		extract($result['vars']);
	}

	/*
	 * Tratando mensagens
	 */
	if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
		$msg = $_SESSION['msg'];
		unset($_SESSION['msg']);
	}

	/*
	 * Visualização
	 */
	$includeHead = null;
	$includeBody = null;
	include("./{$module}-{$action}.php");