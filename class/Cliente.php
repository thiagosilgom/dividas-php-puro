<?php
require_once(dirname(__FILE__) . '/Control.php');

class Cliente extends Control {
	public function list() {
		$list = $this->DB->query("
			SELECT * FROM `clientes` ORDER BY `id` DESC
		")->fetchAll();

		return ['vars' => compact('list')];
	}

	public function create() {
		if (isset($_POST['data'])) {
			$data = $_POST['data'];

			$q = $this->DB->prepare("
				INSERT INTO `clientes`
				(`created`, `updated`, `nome`, `cpf_cnpj`, `data_nascimento`, `endereco`)
				VALUES
				(:created, :updated, :nome, :cpf_cnpj, :data_nascimento, :endereco)
			");
			$response = $q->execute([
				'created'         => $this->now,
				'updated'         => $this->now,
				'nome'            => $data['nome'],
				'cpf_cnpj'        => $data['cpf_cnpj'],
				'data_nascimento' => $this->dateTransform($data['data_nascimento']),
				'endereco'        => $data['endereco']
			]);

			if ($response) {
				$_SESSION['msg'] = ['type' => 'success', 'text' => 'Sucesso ao criar registro'];
			} else {
				$_SESSION['msg'] = ['type' => 'danger', 'text' => 'Ocorreu algum erro inesperado'];
			}

			return ['redirect' => ['action' => 'list']];
		}
	}

	public function edit($id) {
		$data = $this->id($id, 'clientes');
		if ($data === false) {
			$_SESSION['msg'] = ['type' => 'danger', 'text' => 'Registro inexistente'];
			return ['redirect' => ['action' => 'list']];
		}

		if (isset($_POST['data'])) {
			$data = $_POST['data'];

			$q = $this->DB->prepare("
				UPDATE `clientes` SET
				`updated` = :updated, `nome` = :nome, `cpf_cnpj` = :cpf_cnpj, `data_nascimento` = :data_nascimento, `endereco` = :endereco
				WHERE `id` = :id
			");
			$response = $q->execute([
				'updated'         => $this->now,
				'nome'            => $data['nome'],
				'cpf_cnpj'        => $data['cpf_cnpj'],
				'data_nascimento' => $this->dateTransform($data['data_nascimento']),
				'endereco'        => $data['endereco'],
				'id'              => $id
			]);

			if ($response) {
				$_SESSION['msg'] = ['type' => 'success', 'text' => 'Sucesso ao editar registro'];
			} else {
				$_SESSION['msg'] = ['type' => 'danger', 'text' => 'Ocorreu algum erro inesperado'];
			}

			return ['redirect' => ['action' => 'list']];
		}

		return ['vars' => compact('data')];
	}

	public function remove($id) {
		$data = $this->id($id, 'clientes');
		if ($data === false) {
			$_SESSION['msg'] = ['type' => 'danger', 'text' => 'Registro inexistente'];
			return ['redirect' => ['action' => 'list']];
		}

		$q = $this->DB->prepare("
			DELETE FROM `clientes` WHERE `id` = :id
		");
		$q->bindValue(':id', $id);
		$response = $q->execute();

		if ($response) {
			$_SESSION['msg'] = ['type' => 'success', 'text' => 'Sucesso ao excluir registro'];
		} else {
			$_SESSION['msg'] = ['type' => 'danger', 'text' => 'Ocorreu algum erro inesperado'];
		}

		return ['redirect' => ['action' => 'list']];
	}
}