<?php
require_once(dirname(__FILE__) . '/Control.php');
require_once(dirname(__FILE__) . '/Divida.php');

class Dashboard extends Control {
	private function pieChart($list) {
		$pieChart = [
			'Pago' => 0,
			'Aguardando pagamento' => 0,
			'Vencido' => 0
		];

		foreach ($list as $data) {
			$pieChart[$data['status']] += $data['valor'];
		}

		return $pieChart;
	}

	private function barChart($list) {
		$obj = new Divida();

		$barChart = [];
		
		foreach ($list as $data) {
			if ($this->clientePagouEmDia($data)) {
				if (!array_key_exists($data['cliente_id'], $barChart)) {
					$barChart[$data['cliente_id']] = ['count' => 1];
					continue;
				}
				$barChart[$data['cliente_id']]['count']++;
			}
		}

		uasort($barChart, function($a, $b){
			return (-1) * strcmp($a['count'], $b['count']);
		});

		foreach ($barChart as $key => &$value) {
			$cliente = $obj->id($key, 'clientes');
			if (isset($cliente['nome']) && !empty($cliente['nome'])) {
				$value['nome'] = $cliente['nome'];
			}
		} unset($value);

		return $barChart;
	}

	public function clientePagouEmDia($data) {
		return ($data['pago']
			&& !empty($data['data_vencimento']) && $data['data_vencimento'] != '0000-00-00'
			&& !empty($data['data_pagamento']) && $data['data_pagamento'] != '0000-00-00'
			&& $data['data_pagamento'] <= $data['data_vencimento']
			&& !empty($data['cliente_id'])
		);
	}

	public function view() {
		$obj = new Divida();
		$result = $obj->list();
		$list = $result['vars']['list'];

		if (empty($list)) {
			$_SESSION['msg'] = ['type' => 'info', 'text' => 'Nenhuma dívida cadastrada. É necessário que ao menos uma dívida seja cadastrada para a exibição dos gráficos.'];
		}

		$pieChart = $this->pieChart($list);
		$barChart = $this->barChart($list);

		return ['vars' => compact('list', 'pieChart', 'barChart')];
	}
}