<?php

class Control {
	public $DB;
	public $now;
	public $nowDate;

	function __construct() {
		global $DB;
		$this->DB = $DB;
		$this->now = date('Y-m-d H:i:s');
		$split = explode(' ', $this->now);
		$this->nowDate = $split[0];
	}

	public function id($id, $table) {
		$q = $this->DB->prepare("
			SELECT * FROM `{$table}` WHERE `id` = :id
		");
		$q->bindValue(':id', $id);
		$q->execute();
		return $q->fetch();
	}

	public function dateTransform($date) {
		if (empty($date)) {
			return null;
		}
		$split = explode('/', $date);
		return $split[2] . '-' . $split[1] . '-' . $split[0];
	}
}