<?php
require_once(dirname(__FILE__) . '/Control.php');

class Divida extends Control {
	public function status($data) {
		if ($data['pago']) {
			return 'Pago';
		}

		if (empty($data['data_vencimento'])
			|| $data['data_vencimento'] == '0000-00-00'
			|| $this->nowDate <= $data['data_vencimento']
		) {
			return 'Aguardando pagamento';
		}

		return 'Vencido';
	}

	public function list() {
		$list = $this->DB->query("
			SELECT `d`.*, `c`.`nome` AS `cliente_nome`
			FROM `dividas` AS `d`
			LEFT JOIN `clientes` AS `c` ON (`c`.`id` = `d`.`cliente_id`)
			ORDER BY `d`.`id` DESC
		")->fetchAll();

		foreach ($list as &$data) {
			$data['status'] = $this->status($data);
		} unset($data);

		return ['vars' => compact('list')];
	}

	public function create() {
		if (isset($_POST['data'])) {
			$data = $_POST['data'];

			$q = $this->DB->prepare("
				INSERT INTO `dividas`
				(`created`, `updated`, `titulo`, `cliente_id`, `descricao`, `valor`, `data_vencimento`, `pago`, `data_pagamento`)
				VALUES
				(:created, :updated, :titulo, :cliente_id, :descricao, :valor, :data_vencimento, :pago, :data_pagamento)
			");
			$response = $q->execute([
				'created'         => $this->now,
				'updated'         => $this->now,
				'titulo'          => $data['titulo'],
				'cliente_id'      => $data['cliente_id'],
				'descricao'       => $data['descricao'],
				'valor'           => $data['valor'],
				'data_vencimento' => $this->dateTransform($data['data_vencimento']),
				'pago'            => $data['pago'],
				'data_pagamento'  => ($data['pago'] ? $this->dateTransform($data['data_pagamento']) : null)
			]);

			if ($response) {
				$_SESSION['msg'] = ['type' => 'success', 'text' => 'Sucesso ao criar registro'];
			} else {
				$_SESSION['msg'] = ['type' => 'danger', 'text' => 'Ocorreu algum erro inesperado'];
			}

			return ['redirect' => ['action' => 'list']];
		}

		$clientes = $this->DB->query("
			SELECT * FROM `clientes` ORDER BY `nome` ASC, `id` ASC
		")->fetchAll();

		return ['vars' => compact('clientes')];
	}

	public function edit($id) {
		$data = $this->id($id, 'dividas');
		if ($data === false) {
			$_SESSION['msg'] = ['type' => 'danger', 'text' => 'Registro inexistente'];
			return ['redirect' => ['action' => 'list']];
		}

		$data['status'] = $this->status($data);

		if (isset($_POST['data'])) {
			$data = $_POST['data'];

			$q = $this->DB->prepare("
				UPDATE `dividas` SET
				`updated` = :updated, `titulo` = :titulo, `cliente_id` = :cliente_id, `descricao` = :descricao, `valor` = :valor, `data_vencimento` = :data_vencimento, `pago` = :pago, `data_pagamento` = :data_pagamento
				WHERE `id` = :id
			");
			$response = $q->execute([
				'updated'         => $this->now,
				'titulo'          => $data['titulo'],
				'cliente_id'      => $data['cliente_id'],
				'descricao'       => $data['descricao'],
				'valor'           => $data['valor'],
				'data_vencimento' => $this->dateTransform($data['data_vencimento']),
				'pago'            => $data['pago'],
				'data_pagamento'  => ($data['pago'] ? $this->dateTransform($data['data_pagamento']) : null),
				'id'              => $id
			]);

			if ($response) {
				$_SESSION['msg'] = ['type' => 'success', 'text' => 'Sucesso ao editar registro'];
			} else {
				$_SESSION['msg'] = ['type' => 'danger', 'text' => 'Ocorreu algum erro inesperado'];
			}

			return ['redirect' => ['action' => 'list']];
		}

		$clientes = $this->DB->query("
			SELECT * FROM `clientes` ORDER BY `nome` ASC, `id` ASC
		")->fetchAll();

		return ['vars' => compact('data', 'clientes')];
	}

	public function remove($id) {
		$data = $this->id($id, 'dividas');
		if ($data === false) {
			$_SESSION['msg'] = ['type' => 'danger', 'text' => 'Registro inexistente'];
			return ['redirect' => ['action' => 'list']];
		}

		$q = $this->DB->prepare("
			DELETE FROM `dividas` WHERE `id` = :id
		");
		$q->bindValue(':id', $id);
		$response = $q->execute();

		if ($response) {
			$_SESSION['msg'] = ['type' => 'success', 'text' => 'Sucesso ao excluir registro'];
		} else {
			$_SESSION['msg'] = ['type' => 'danger', 'text' => 'Ocorreu algum erro inesperado'];
		}

		return ['redirect' => ['action' => 'list']];
	}
}