# README #

Seguem algumas instruções e observações

### Banco de Dados ###

* Crie primeiramente o banco de dados com qualquer nome que queira: **CREATE DATABASE meu_banco;**
* Com esse banco em uso, importe o arquivo **db/import.sql**
* Altere as configurações do banco no arquivo **db/connection.php**
* A modelagem do banco está no arquivo **db/modelagem.jpg**

Obs: o campo **cpf_cnpj** foi mudado para string, pois como número inteiro não teríamos como reconhecer se o cliente tem um CPF ou CNPJ já que zeros à esquerda são possíveis. Por exemplo: suponhamos 3517904112, não conseguiríamos saber se é o CPF **035.179.041-12** ou o CNPJ **00.003.517/9041-12**. Por esse motivo o campo como string foi escolhido.

Um exemplo de problema com zero à esquerda em CPF: https://economia.uol.com.br/noticias/redacao/2020/04/27/auxilio-r-600-zero-cpf-some-confirmacao-do-pedido.htm

### Testes Unitários ###

Para executar os testes, vá até a pasta raiz do projeto pelo terminal, e depois digite:

- **php test/ClienteTest.php**
- **php test/DividaTest.php**
- **php test/DashboardTest.php**
- **php test/ControlTest.php**