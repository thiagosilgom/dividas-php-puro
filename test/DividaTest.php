<?php
require_once(dirname(dirname(__FILE__)) . '/db/connection.php');
require_once(dirname(dirname(__FILE__)) . '/class/Divida.php');

class DividaTest {
	public function testStatusPago() {
		$obj = new Divida();

		$this->nowDate = '2021-01-02';
		$data = [
			'data_vencimento' => '2021-01-03',
			'pago' => 1,
			'data_pagamento' => '2021-01-01'
		];

		if ($obj->status($data) === 'Pago') {
			echo 'testStatusPago => SUCCESS' . PHP_EOL;
			return true;
		}
		echo 'testStatusPago => ERROR' . PHP_EOL;
		return false;
	}

	public function testStatusAguardandoPagamento() {
		$obj = new Divida();

		$this->nowDate = '2021-02-05';
		$data = [
			'data_vencimento' => '2021-03-20',
			'pago' => 0,
			'data_pagamento' => null
		];

		if ($obj->status($data) === 'Aguardando pagamento') {
			echo 'testStatusAguardandoPagamento => SUCCESS' . PHP_EOL;
			return true;
		}
		echo 'testStatusAguardandoPagamento => ERROR' . PHP_EOL;
		return false;
	}

	public function testStatusVencido() {
		$obj = new Divida();

		$this->nowDate = '2021-02-07';
		$data = [
			'data_vencimento' => '2021-01-20',
			'pago' => 0,
			'data_pagamento' => null
		];

		if ($obj->status($data) === 'Vencido') {
			echo 'testStatusVencido => SUCCESS' . PHP_EOL;
			return true;
		}
		echo 'testStatusVencido => ERROR' . PHP_EOL;
		return false;
	}

	private function criarCliente() {
		$obj = new Divida();

		$q = $obj->DB->prepare("
			INSERT INTO `clientes`
			(`created`, `updated`, `nome`, `cpf_cnpj`, `data_nascimento`, `endereco`)
			VALUES
			(:created, :updated, :nome, :cpf_cnpj, :data_nascimento, :endereco)
		");
		
		return $q->execute([
			'created'         => $obj->now,
			'updated'         => $obj->now,
			'nome'            => '###TestDividaClienteCreate###',
			'cpf_cnpj'        => '035.179.041-12',
			'data_nascimento' => '1990-04-02',
			'endereco'        => 'Av. Afonso Pena, 1010, Centro'
		]);
	}

	private function getClienteId() {
		$obj = new Divida();

		$cliente = $obj->DB->query("SELECT * FROM `clientes` WHERE `nome` = '###TestDividaClienteCreate###'")->fetch();

		return $cliente['id'];
	}

	private function removerCliente() {
		$obj = new Divida();

		$q = $obj->DB->prepare("DELETE FROM `clientes` WHERE `id` = :id");
		$q->bindValue(':id', $this->getClienteId());

		return $q->execute();
	}

	public function testCreate() {
		$obj = new Divida();

		$this->criarCliente();

		$_POST['data'] = [
			'titulo' => '###TestDividaCreate###',
			'cliente_id' => $this->getClienteId(),
			'descricao' => 'Descricao da divida',
			'valor' => '150.00',
			'data_vencimento' => '02/03/2021',
			'pago' => 0,
			'data_pagamento' => ''
		];
		$obj->create();

		$response = $obj->DB->query("SELECT * FROM `dividas` WHERE `titulo` = '###TestDividaCreate###'")->fetch();
		if ($response !== false) {
			echo 'testCreate => SUCCESS' . PHP_EOL;
			return true;
		}
		echo 'testCreate => ERROR' . PHP_EOL;
		return false;
	}

	public function testEdit() {
		$obj = new Divida();

		$divida = $obj->DB->query("SELECT * FROM `dividas` WHERE `titulo` = '###TestDividaCreate###'")->fetch();

		$_POST['data'] = [
			'titulo' => '###TestDividaEdit###',
			'cliente_id' => $this->getClienteId(),
			'descricao' => 'Alterando descricao da divida',
			'valor' => '300.00',
			'data_vencimento' => '27/01/2021',
			'pago' => 1,
			'data_pagamento' => '09/02/2021'
		];
		$obj->edit($divida['id']);

		$response = $obj->DB->query("SELECT * FROM `dividas` WHERE `id` = {$divida['id']}")->fetch();
		if ($response['titulo'] === '###TestDividaEdit###'
			&& $response['descricao'] === 'Alterando descricao da divida'
			&& $response['valor'] === '300.00'
			&& $response['data_vencimento'] === '2021-01-27'
			&& $response['pago'] === '1'
			&& $response['data_pagamento'] === '2021-02-09'
		) {
			echo 'testEdit => SUCCESS' . PHP_EOL;
			return true;
		}
		echo 'testEdit => ERROR' . PHP_EOL;
		return false;
	}

	public function testRemove() {
		$obj = new Divida();

		$divida = $obj->DB->query("SELECT * FROM `dividas` WHERE `titulo` = '###TestDividaEdit###'")->fetch();
		$obj->remove($divida['id']);

		$response = $obj->DB->query("SELECT * FROM `dividas` WHERE `id` = {$divida['id']}")->fetch();
		if ($response === false) {
			echo 'testRemove => SUCCESS' . PHP_EOL;
			$this->removerCliente();
			return true;
		}
		echo 'testRemove => ERROR' . PHP_EOL;
		$this->removerCliente();
		return false;
	}
}

$dividaTest = new DividaTest();
$dividaTest->testStatusPago();
$dividaTest->testStatusAguardandoPagamento();
$dividaTest->testStatusVencido();
$dividaTest->testCreate()
&& $dividaTest->testEdit()
&& $dividaTest->testRemove();