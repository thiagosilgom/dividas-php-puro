<?php
require_once(dirname(dirname(__FILE__)) . '/db/connection.php');
require_once(dirname(dirname(__FILE__)) . '/class/Cliente.php');

class ClienteTest {
	public function testCreate() {
		$obj = new Cliente();

		$_POST['data'] = [
			'nome' => '###TestClienteCreate###',
			'cpf_cnpj' => '035.179.041-12',
			'data_nascimento' => '02/04/1990',
			'endereco' => 'Av. Afonso Pena, 1010, Centro'
		];
		$obj->create();

		$response = $obj->DB->query("SELECT * FROM `clientes` WHERE `nome` = '###TestClienteCreate###'")->fetch();
		if ($response !== false) {
			echo 'testCreate => SUCCESS' . PHP_EOL;
			return true;
		}
		echo 'testCreate => ERROR' . PHP_EOL;
		return false;
	}

	public function testEdit() {
		$obj = new Cliente();

		$cliente = $obj->DB->query("SELECT * FROM `clientes` WHERE `nome` = '###TestClienteCreate###'")->fetch();

		$_POST['data'] = [
			'nome' => '###TestClienteEdit###',
			'cpf_cnpj' => '111.111.111-11',
			'data_nascimento' => '04/02/2000',
			'endereco' => 'Rua João de Andrade, 678, Santo Amaro'
		];
		$obj->edit($cliente['id']);

		$response = $obj->DB->query("SELECT * FROM `clientes` WHERE `id` = {$cliente['id']}")->fetch();
		if ($response['nome'] === '###TestClienteEdit###'
			&& $response['cpf_cnpj'] === '111.111.111-11'
			&& $response['data_nascimento'] === '2000-02-04'
			&& $response['endereco'] === 'Rua João de Andrade, 678, Santo Amaro'
		) {
			echo 'testEdit => SUCCESS' . PHP_EOL;
			return true;
		}
		echo 'testEdit => ERROR' . PHP_EOL;
		return false;
	}

	public function testRemove() {
		$obj = new Cliente();

		$cliente = $obj->DB->query("SELECT * FROM `clientes` WHERE `nome` = '###TestClienteEdit###'")->fetch();
		$obj->remove($cliente['id']);

		$response = $obj->DB->query("SELECT * FROM `clientes` WHERE `id` = {$cliente['id']}")->fetch();
		if ($response === false) {
			echo 'testRemove => SUCCESS' . PHP_EOL;
			return true;
		}
		echo 'testRemove => ERROR' . PHP_EOL;
		return false;
	}
}

$clienteTest = new ClienteTest();
$clienteTest->testCreate()
&& $clienteTest->testEdit()
&& $clienteTest->testRemove();