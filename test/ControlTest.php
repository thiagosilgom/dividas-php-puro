<?php
require_once(dirname(dirname(__FILE__)) . '/db/connection.php');
require_once(dirname(dirname(__FILE__)) . '/class/Control.php');

class ControlTest {
	public function testDateTransform() {
		$obj = new Control();

		if ($obj->dateTransform('02/04/1990') === '1990-04-02') {
			echo 'testDateTransform => SUCCESS' . PHP_EOL;
			return true;
		}
		echo 'testDateTransform => ERROR' . PHP_EOL;
		return false;
	}

	public function testEmptyDateTransform() {
		$obj = new Control();

		if ($obj->dateTransform('') === null) {
			echo 'testEmptyDateTransform => SUCCESS' . PHP_EOL;
			return true;
		}
		echo 'testEmptyDateTransform => ERROR' . PHP_EOL;
		return false;
	}
}

$dashboardTest = new ControlTest();
$dashboardTest->testDateTransform();
$dashboardTest->testEmptyDateTransform();