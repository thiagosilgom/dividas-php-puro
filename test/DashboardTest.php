<?php
require_once(dirname(dirname(__FILE__)) . '/db/connection.php');
require_once(dirname(dirname(__FILE__)) . '/class/Dashboard.php');

class DashboardTest {
	public function testClientePagouEmDia() {
		$obj = new Dashboard();

		$data = [
			'cliente_id' => 1,
			'data_vencimento' => '2021-01-03',
			'pago' => 1,
			'data_pagamento' => '2021-01-01'
		];

		if ($obj->clientePagouEmDia($data) === true) {
			echo 'testClientePagouEmDia => SUCCESS' . PHP_EOL;
			return true;
		}
		echo 'testClientePagouEmDia => ERROR' . PHP_EOL;
		return false;
	}

	public function testClienteNaoPagouEmDia() {
		$obj = new Dashboard();

		$data = [
			'cliente_id' => 1,
			'data_vencimento' => '2021-01-20',
			'pago' => 1,
			'data_pagamento' => '2021-02-09'
		];

		if ($obj->clientePagouEmDia($data) === false) {
			echo 'testClienteNaoPagouEmDia => SUCCESS' . PHP_EOL;
			return true;
		}
		echo 'testClienteNaoPagouEmDia => ERROR' . PHP_EOL;
		return false;
	}
}

$dashboardTest = new DashboardTest();
$dashboardTest->testClientePagouEmDia();
$dashboardTest->testClienteNaoPagouEmDia();