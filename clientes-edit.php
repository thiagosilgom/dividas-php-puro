<?php $includeBody = 'clientes-body.php'; ?>
<?php include('header.php'); ?>

<form action="<?php echo url(['action' => 'edit', 'id' => $id]); ?>" method="post">
	<div class="row">
		<div class="form-group col-md-8 offset-md-2">
			<label for="nome">Nome *</label>
			<input type="text" class="form-control" id="nome" name="data[nome]" value="<?php echo h($data['nome']); ?>" required>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-4 offset-md-2">
			<label for="cpf_cnpj">CPF/CNPJ *</label>
			<input type="text" class="form-control" id="cpf_cnpj" name="data[cpf_cnpj]" value="<?php echo h($data['cpf_cnpj']); ?>" required>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-4 offset-md-2">
			<label for="data_nascimento">Data de Nascimento</label>
			<input type="text" class="form-control" id="data_nascimento" name="data[data_nascimento]" value="<?php echo showDate($data['data_nascimento']); ?>">
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-8 offset-md-2">
			<label for="endereco">Endereço</label>
			<input type="text" class="form-control" id="endereco" name="data[endereco]" value="<?php echo h($data['endereco']); ?>">
		</div>
	</div>
	<div class="row">
		<div class="col-md-8 offset-md-2">
			<button type="submit" class="btn btn-primary float-right">Salvar</button>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8 offset-md-2">
			<strong>Data de Criação:</strong> <?php echo showDatetime($data['created']); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8 offset-md-2">
			<strong>Última Atualização:</strong> <?php echo showDatetime($data['updated']); ?>
		</div>
	</div>
</form>

<?php include('footer.php'); ?>