<script type="text/javascript" src="./assets/js/jquery.inputmask.bundle.min.js"></script>
<script>
	$(document).ready(function(){
		$('#cpf_cnpj').inputmask({
            mask: ['999.999.999-99', '99.999.999/9999-99'],
            keepStatic: true,
            clearIncomplete: true
        });
        $('#data_nascimento').inputmask({
            mask: '99/99/9999',
            keepStatic: true,
            clearIncomplete: true
        });
	});
</script>