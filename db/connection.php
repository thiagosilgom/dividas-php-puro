<?php
	define('DB_HOST', 'localhost');
	define('DB_USER', 'root');
	define('DB_PASS', '');
	define('DB_NAME', 'desafio_receiv');
	define('DB_PORT', 3306);

	try {
	    $DB = new PDO('mysql:host=' . DB_HOST . (!empty(DB_PORT) ? ';port=' . DB_PORT : '') . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
	}
	catch (PDOException $e) {
	    echo 'Erro ao conectar com o MySQL: ' . $e->getMessage();
	    exit();
	}