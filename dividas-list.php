<?php include('header.php'); ?>

<div class="table-responsive">
	<table class="table table-sm">
		<thead>
			<tr>
				<th>#</th>
				<th>Título</th>
				<th>Cliente</th>
				<th>Valor</th>
				<th>Data de Vencimento</th>
				<th>Status</th>
				<th>Ações</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($list as $data) : ?>
				<tr class="<?php echo ($data['status'] == 'Pago' ? 'status-green' : ($data['status'] == 'Vencido' ? 'status-red' : '')); ?>">
					<td><?php echo $data['id']; ?></td>
					<td><?php echo h($data['titulo']); ?></td>
					<td><?php echo h($data['cliente_nome']); ?></td>
					<td><?php echo showMoney($data['valor']); ?></td>
					<td><?php echo showDate($data['data_vencimento']); ?></td>
					<td><?php echo $data['status']; ?></td>
					<td>
						<a class="btn btn-success" href="<?php echo url(['action' => 'edit', 'id' => $data['id']]); ?>">Editar</a>
						<a class="btn btn-danger" href="<?php echo url(['action' => 'remove', 'id' => $data['id']]); ?>" onclick="if (confirm('Deseja realmente remover este registro?')) { return true; } return false;">Excluir</a>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>

<?php include('footer.php'); ?>