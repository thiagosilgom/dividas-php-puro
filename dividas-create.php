<?php $includeBody = 'dividas-body.php'; ?>
<?php include('header.php'); ?>

<form action="<?php echo url(['action' => 'create']); ?>" method="post">
	<div class="row">
		<div class="form-group col-md-8 offset-md-2">
			<label for="titulo">Título *</label>
			<input type="text" class="form-control" id="titulo" name="data[titulo]" required>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-8 offset-md-2">
			<label for="cliente_id">Cliente *</label>
			<select type="text" class="form-control" id="cliente_id" name="data[cliente_id]" required>
				<option value=""></option>
				<?php foreach ($clientes as $cliente) : ?>
					<option value="<?php echo $cliente['id']; ?>"><?php echo h($cliente['nome']); ?></option>
				<?php endforeach; ?>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-8 offset-md-2">
			<label for="descricao">Descrição</label>
			<textarea type="text" class="form-control" id="descricao" name="data[descricao]" rows="3"></textarea>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-4 offset-md-2">
			<label for="valor">Valor *</label>
			<input type="number" step="0.01" class="form-control" id="valor" name="data[valor]" required>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-4 offset-md-2">
			<label for="data_vencimento">Data de Vencimento *</label>
			<input type="text" class="form-control" id="data_vencimento" name="data[data_vencimento]" required>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-4 offset-md-2">
			<label for="pago">Pago?</label>
			<select type="text" class="form-control" id="pago" name="data[pago]" required>
				<option value="1">Sim</option>
				<option value="0" selected>Não</option>
			</select>
		</div>
	</div>
	<div class="row" id="field_data_pagamento">
		<div class="form-group col-md-4 offset-md-2">
			<label for="data_pagamento">Data de Pagamento *</label>
			<input type="text" class="form-control" id="data_pagamento" name="data[data_pagamento]" value="<?php echo date('d/m/Y'); ?>">
		</div>
	</div>
	<div class="row">
		<div class="col-md-8 offset-md-2">
			<button type="submit" class="btn btn-primary float-right">Salvar</button>
		</div>
	</div>
</form>

<?php include('footer.php'); ?>