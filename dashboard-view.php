<?php $includeHead = 'dashboard-head.php'; ?>
<?php include('header.php'); ?>

<?php if (!empty($list)) : ?>
	<div class="row">
		<div class="col-md-6 offset-md-3">
			<div id="piechart" style="width: 100%; min-height: 600px;"></div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6 offset-md-3">
			<?php if (!empty($barChart)) : ?>
				<div id="barchart" style="width: 100%; min-height: 600px;"></div>
			<?php else : ?>
				<div class="alert alert-warning" role="alert">
					Não foi possível exibir o gráfico dos clientes que mais pagam em dia, pois ainda não há pagamento em dia
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>

<?php include('footer.php'); ?>