<?php if (!empty($msg)) : ?>
	<div class="alert alert-<?php echo $msg['type']; ?>" role="alert">
		<?php echo $msg['text']; ?>
	</div>
<?php endif; ?>