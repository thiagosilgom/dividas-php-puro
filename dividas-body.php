<script type="text/javascript" src="./assets/js/jquery.inputmask.bundle.min.js"></script>
<script>
	$(document).ready(function(){
        $('#data_vencimento').inputmask({
            mask: '99/99/9999',
            keepStatic: true,
            clearIncomplete: true
        });
        $('#data_pagamento').inputmask({
            mask: '99/99/9999',
            keepStatic: true,
            clearIncomplete: true
        });

        $('#pago').change(function(){
            if ($(this).val() == 1) {
                $('#field_data_pagamento').show();
                $('#data_pagamento').attr('required', 'required');
            } else {
                $('#field_data_pagamento').hide();
                $('#data_pagamento').removeAttr('required');
            }
        }).change();
	});
</script>